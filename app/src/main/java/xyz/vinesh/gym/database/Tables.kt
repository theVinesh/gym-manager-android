package xyz.vinesh.gym.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import org.jetbrains.annotations.NotNull
import xyz.vinesh.gym.enums.PaymentMethods
import java.util.*

/**
 * Created by vineshraju on 18/10/17.
 */
//todo remove age
@Entity(indices = [Index(value = "mobile", unique = true), Index(value = "id", unique = true)])
data class Trainee(@NotNull @PrimaryKey var id: String, @ColumnInfo var name: String, @ColumnInfo var age: Int,
                   @ColumnInfo var dob: String, @ColumnInfo var sex: String, @ColumnInfo var mobile: String,
                   @ColumnInfo var doj: Date, @ColumnInfo var membershipType: Int, @ColumnInfo var weight: Float,
                   @ColumnInfo var height: Float, @ColumnInfo var expiresOn: Date,
                   @ColumnInfo var paymentPending: Float) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            Date(parcel.readLong()),
            parcel.readInt(),
            parcel.readFloat(),
            parcel.readFloat(),
            Date(parcel.readLong()),
            parcel.readFloat())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeInt(age)
        parcel.writeString(dob)
        parcel.writeString(sex)
        parcel.writeString(mobile)
        parcel.writeLong(doj.time)
        parcel.writeInt(membershipType)
        parcel.writeFloat(weight)
        parcel.writeFloat(height)
        parcel.writeLong(expiresOn.time)
        parcel.writeFloat(paymentPending)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Trainee> {
        override fun createFromParcel(parcel: Parcel): Trainee {
            return Trainee(parcel)
        }

        override fun newArray(size: Int): Array<Trainee?> {
            return arrayOfNulls(size)
        }
    }

}

/*
@Entity(foreignKeys = arrayOf(ForeignKey(entity = Trainee::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("trainee_id"))))*/
@Entity
data class Payment(@NotNull @PrimaryKey(autoGenerate = true) var id: Long? = null, @ColumnInfo var trainee_id: String, @ColumnInfo var payed_amount: Float,
                   @ColumnInfo var paymentMethod: PaymentMethods, @ColumnInfo var paidOn: Date, @ColumnInfo var forUntill: Date)

@Entity
data class Membership(@NotNull @PrimaryKey(autoGenerate = true) var id: Long? = null, @ColumnInfo var name: String, @ColumnInfo var price: Float,
                      @ColumnInfo var durationInMonths: Int) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readString(),
            parcel.readFloat(),
            parcel.readInt())

    constructor() : this(
            -1,
            "",
            0f,
            -1)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(name)
        parcel.writeFloat(price)
        parcel.writeInt(durationInMonths)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Membership> {
        override fun createFromParcel(parcel: Parcel): Membership {
            return Membership(parcel)
        }

        override fun newArray(size: Int): Array<Membership?> {
            return arrayOfNulls(size)
        }
    }
}


fun List<Membership>.toStringArray() = Array(this.size,
        {
            this[it].name + if (this[it].durationInMonths <= 0) "" else {
                " - ${this[it].durationInMonths} month${if (this[it].durationInMonths > 1) "s" else ""}"
            }
        })
