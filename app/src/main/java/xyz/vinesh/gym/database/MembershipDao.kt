package xyz.vinesh.gym.database

import android.arch.persistence.room.*

/**
 * Created by vineshraju on 18/10/17.
 */
@Dao
interface MembershipDao {
    @Insert
    fun insert(vararg membership: Membership)

    @Delete
    fun delete(membership: Membership)

    @Update
    fun update(membership: Membership)

    @Query("SELECT * FROM Membership")
    fun getAll(): List<Membership>

    @Query("SELECT * FROM Membership WHERE id = :id LIMIT 1")
    fun get(id: Long): Membership?
}