package xyz.vinesh.gym.database

import android.arch.persistence.room.TypeConverter
import xyz.vinesh.gym.enums.PaymentMethods
import java.util.*


/**
 * Created by vineshraju on 18/10/17.
 */
class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?) = if (value != null) Date(value) else null

    @TypeConverter
    fun dateToTimestamp(date: Date?) = date?.time

    @TypeConverter
    fun paymentMethodToInt(methods: PaymentMethods?) = methods?.ordinal

    @TypeConverter
    fun intToPaymentMethod(method: Int?) = if (method != null) PaymentMethods.values()[method] else null
}