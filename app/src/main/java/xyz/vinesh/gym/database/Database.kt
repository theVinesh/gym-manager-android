package xyz.vinesh.gym.database

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Room
import android.arch.persistence.room.migration.Migration
import android.content.Context

/**
 * Created by vineshraju on 30/10/17.
 */

object Database {
    private val appDB: AppDB? = null
    fun create(context: Context) =
            appDB ?: Room.databaseBuilder(context.applicationContext, AppDB::class.java, "gym_db")
                    .allowMainThreadQueries()
                    .addMigrations(object : Migration(1, 2) {
                        override fun migrate(database: SupportSQLiteDatabase) {
                            val ALTERQUERY = "ALTER TABLE Trainee ADD COLUMN dob TEXT NOT NULL DEFAULT 'null'"
                            database.execSQL(ALTERQUERY)
                        }

                    })
                    .build()
}