package xyz.vinesh.gym.database

import android.arch.persistence.room.*
import com.google.firebase.perf.metrics.AddTrace

/**
 * Created by vineshraju on 18/10/17.
 */
@Dao
interface TraineeDao {

    @AddTrace(name = "traineeInsertTrace")
    @Insert
    fun insert(vararg trainees: Trainee)

    @AddTrace(name = "traineeDeleteTrace")
    @Delete
    fun delete(trainee: Trainee)

    @AddTrace(name = "traineeGetAllTrace")
    @Query("SELECT * FROM Trainee ORDER BY name ASC")
    fun getAll(): List<Trainee>

    @AddTrace(name = "traineeSearchTrace")
    @Query("SELECT * FROM Trainee WHERE id LIKE :query OR name LIKE :query OR mobile LIKE :query ORDER BY name ASC")
    fun search(query: String): List<Trainee>

    @AddTrace(name = "traineeWithExpiredMembershipTrace")
    @Query("SELECT * FROM Trainee WHERE expiresOn < :todaysDateStamp ORDER BY name ASC")
    fun withExpiredMembership(todaysDateStamp: Long): List<Trainee>

    @AddTrace(name = "traineeWithPaymentPendingTrace")
    @Query("SELECT * FROM Trainee WHERE paymentPending > 0 ORDER BY name ASC")
    fun withPaymentPending(): List<Trainee>

    @AddTrace(name = "traineeUpdateTrace")
    @Update
    fun update(trainee: Trainee)

    @AddTrace(name = "traineeGetByIdTrace")
    @Query("SELECT * FROM Trainee WHERE id = :id")
    fun getBy(id: String): Trainee
}