package xyz.vinesh.gym.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

/**
 * Created by vineshraju on 18/10/17.
 */
@Dao
interface PaymentDao {
    @Insert
    fun insert(vararg payment: Payment)

    @Delete
    fun delete(payment: Payment)

    @Query("SELECT * FROM Payment")
    fun getAll(): List<Payment>

    @Query("SELECT * FROM Payment WHERE trainee_id = :traineeId AND forUntill = :forUntill")
    fun getAll(traineeId: String, forUntill: Long): List<Payment>

    @Query("SELECT * FROM Payment WHERE trainee_id = :traineeId ORDER BY paidOn DESC")
    fun getAll(traineeId: String): List<Payment>
}