package xyz.vinesh.gym.activity

import android.os.Bundle
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.ContentViewEvent
import kotlinx.android.synthetic.main.activity_change_log.*
import kotlinx.android.synthetic.main.content_change_log.*
import xyz.vinesh.gym.R


class ChangeLogActivity : EnhancedActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_log)
        setSupportActionBar(toolbar)
        tvChangeLog.text = resources.openRawResource(R.raw.changelog).bufferedReader().readText()
        Answers.getInstance().logContentView(ContentViewEvent()
                .putContentName(this::class.java.simpleName))
    }
}
