package xyz.vinesh.gym.activity

import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.widget.TextViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_home_screen.*
import kotlinx.android.synthetic.main.content_home_page.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.yesButton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xyz.vinesh.gym.R
import xyz.vinesh.gym.adapter.HomeScreenListAdapter
import xyz.vinesh.gym.database.Database
import xyz.vinesh.gym.enums.Constants
import xyz.vinesh.gym.networking.NetworkHandler
import xyz.vinesh.gym.networking.responses.APIResponse
import xyz.vinesh.gym.util.Analytics
import xyz.vinesh.gym.util.PrefsUtils


class HomeScreenActivity : EnhancedActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        val verificationKey = PrefsUtils.getString(Constants.verification_keys)
        if (verificationKey.isNotBlank()) {
            val verifyCall = NetworkHandler.create().verifyProductKey(verificationKey)
            verifyCall.enqueue(object : Callback<APIResponse> {
                override fun onFailure(call: Call<APIResponse>?, t: Throwable?) {
                    t?.printStackTrace()
                }

                override fun onResponse(call: Call<APIResponse>?, response: Response<APIResponse>?) {
                    when (response?.code()) {
                        200 -> {
                            if (response.body()?.status == true) {
                                //Valid key
                                Analytics.logEvent(Analytics.Events.VERIFIED, Analytics.Attributes(Analytics.AttributesKey.VERIFICATION_KEY, verificationKey))
                                if (PrefsUtils.getString(Constants.gym_name) != response.body()?.name)
                                    PrefsUtils.put(Constants.gym_name, response.body()?.name ?: resources.getString(R.string.app_name))
                                logAppToFabric(verificationKey, response.body()?.name ?: "unknown_user")
                            } else {
                                //not enabled/used too many times or No key exist
                                showExpiredPage()
                                Analytics.logEvent(Analytics.Events.UNVERIFIED, Analytics.Attributes(Analytics.AttributesKey.VERIFICATION_KEY, verificationKey))
                            }
                        }

                        else -> {
                            Analytics.logEvent(Analytics.Events.NO_CONNECTION_TO_VERIFY)
                        }

                    }
                }
            })
            setContentView(R.layout.activity_home_screen)
            setSupportActionBar(toolbar)
            actionBarTitle.text = PrefsUtils.getString(Constants.gym_name)
            TextViewCompat.setAutoSizeTextTypeWithDefaults(actionBarTitle, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM)

            fabAddNew.setOnClickListener {
                Analytics.logEvent(Analytics.Events.ADD_MEMBER)
                if (Database.create(this).membership().getAll().isNotEmpty())
                    startActivity(intentFor<AddTraineeActivity>())
                else alert {
                    titleResource = R.string.title_add_member_empty_membership_title
                    messageResource = R.string.title_add_member_empty_membership_warning
                    yesButton {
                        Analytics.logEvent(Analytics.Events.ADD_MEMBERSHIP_PLAN,
                                Analytics.Attributes(Analytics.AttributesKey.SCREEN, this::class.java.simpleName)
                        )
                        startActivity(intentFor<MembershipsListingActivity>())
                    }
                }.show()
            }
        } else {
            //no verification key
            Analytics.logEvent(Analytics.Events.NO_VERIFICATION_KEY)
            showVerifyKeyPage()
        }
    }

    private fun showVerifyKeyPage() {
        finish()
        startActivity(intentFor<VerifyKeyActivity>())
    }

    private fun showExpiredPage() {
        finish()
        startActivity(intentFor<VerifyKeyActivity>())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.home_menu, menu)
        return true
    }

    override fun onResume() {
        super.onResume()
        val expiredMembers = Database.create(this).trainee().withExpiredMembership(System.currentTimeMillis())
        val paymentPendingMembers = Database.create(this).trainee().withPaymentPending()
        val allMembers = Database.create(this).trainee().getAll()
        if (expiredMembers.isEmpty() and paymentPendingMembers.isEmpty()) {
            rvExpiresList.visibility = View.GONE
            textView.visibility = View.GONE
            rvPaymentPendingList.visibility = View.GONE
            textView2.visibility = View.GONE

            llEmptyStateContainer.visibility = View.VISIBLE
            if (allMembers.isEmpty()) {
                //ivHomeEmptyIllustration.setImageResource()
                tvHomeEmptyIllustration.setText(R.string.desc_no_users)
            } else {
                //ivHomeEmptyIllustration.setImageResource()
                tvHomeEmptyIllustration.setText(R.string.desc_empty_home_screen)
            }
            return
        } else {
            llEmptyStateContainer.visibility = View.GONE
        }
        if (expiredMembers.isNotEmpty()) {
            rvExpiresList.visibility = View.VISIBLE
            textView.visibility = View.VISIBLE
            rvExpiresList.layoutManager = LinearLayoutManager(this)
            rvExpiresList.adapter = HomeScreenListAdapter(
                    expiredMembers,
                    { view, trainee ->
                        val intent = intentFor<TraineeDetailsActivity>(Constants.TRAINEE_DETAILS_POJO.name to trainee)
                        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, resources.getString(R.string.title_name))
                        startActivity(intent, options.toBundle())
                        Analytics.logEvent(Analytics.Events.EXPIRED_MEMBER_CLICKED, Analytics.Attributes(Analytics.AttributesKey.SCREEN, this::class.java.simpleName))
                    })
        } else {
            rvExpiresList.visibility = View.GONE
            textView.visibility = View.GONE
        }
        if (paymentPendingMembers.isNotEmpty()) {
            rvPaymentPendingList.visibility = View.VISIBLE
            textView2.visibility = View.VISIBLE
            rvPaymentPendingList.layoutManager = LinearLayoutManager(this)
            rvPaymentPendingList.adapter = HomeScreenListAdapter(
                    paymentPendingMembers,
                    { view, trainee ->
                        val intent = intentFor<TraineeDetailsActivity>(Constants.TRAINEE_DETAILS_POJO.name to trainee)
                        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, resources.getString(R.string.title_name))
                        startActivity(intent, options.toBundle())
                        Analytics.logEvent(Analytics.Events.NOT_PAYED_MEMBER_CLICKED, Analytics.Attributes(Analytics.AttributesKey.SCREEN, this::class.java.simpleName))

                    }, true)
        } else {
            rvPaymentPendingList.visibility = View.GONE
            textView2.visibility = View.GONE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.mSearch -> {
                startActivity(intentFor<SearchActivity>())
                Analytics.logEvent(Analytics.Events.SEARCH_CLICKED)
                return true
            }
            R.id.mSettings -> {
                Analytics.logEvent(Analytics.Events.SETTINGS_CLICKED)
                startActivity(intentFor<SettingActivity>())
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun logAppToFabric(key: String, name: String) {
        Crashlytics.setString(Constants.verification_keys.name, key)
        Crashlytics.setUserName(name)
    }

}
