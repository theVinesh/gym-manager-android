package xyz.vinesh.gym.activity

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.KeyEvent
import android.view.View
import android.widget.TextView
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.SignUpEvent
import kotlinx.android.synthetic.main.activity_verify_key.*
import kotlinx.android.synthetic.main.content_verify_key.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xyz.vinesh.gym.R
import xyz.vinesh.gym.enums.Constants
import xyz.vinesh.gym.networking.NetworkHandler
import xyz.vinesh.gym.networking.responses.APIResponse
import xyz.vinesh.gym.util.Analytics
import xyz.vinesh.gym.util.PrefsUtils


class VerifyKeyActivity : EnhancedActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_key)
        setSupportActionBar(toolbar)

        etVerificationCode.setOnEditorActionListener { _: TextView?, _: Int?, _: KeyEvent? ->
            verify(etVerificationCode.text.toString())
            true
        }
        fabVerify.setOnClickListener { _ ->
            verify(etVerificationCode.text.toString())
        }
    }

    private fun verify(verificationKey: String) {
        if (etVerificationCode.text.isBlank()) {
            etVerificationCode.error = resources.getString(R.string.error_key_invalid)
            return
        }

        pbProgress.visibility = View.VISIBLE

        val updateUsageCall = NetworkHandler.create().updateProductKeyUsage(verificationKey)
        updateUsageCall.enqueue(object : Callback<APIResponse> {
            override fun onFailure(call: Call<APIResponse>?, t: Throwable?) {
                t?.printStackTrace()
                pbProgress.visibility = View.GONE
                toast(R.string.error_default)
            }

            override fun onResponse(call: Call<APIResponse>?, response: Response<APIResponse>?) {
                pbProgress.visibility = View.GONE
                when (response?.code()) {
                    200 -> {
                        if (response.body()?.status == true) {
                            //valid key
                            PrefsUtils.put(Constants.gym_name, response.body()?.name ?: resources.getString(R.string.app_name))
                            PrefsUtils.put(Constants.verification_keys, verificationKey)
                            finish()
                            Answers.getInstance().logSignUp(SignUpEvent()
                                    .putCustomAttribute(Analytics.AttributesKey.VERIFICATION_KEY.name, verificationKey)
                                    .putSuccess(true))
                            startActivity(intentFor<HomeScreenActivity>())
                        } else {
                            //not enabled/used too many times or No key exist
                            Answers.getInstance().logSignUp(SignUpEvent()
                                    .putCustomAttribute(Analytics.AttributesKey.VERIFICATION_KEY.name, verificationKey)
                                    .putCustomAttribute(Analytics.AttributesKey.STATUS.name, response.body()?.message)
                                    .putSuccess(false))
                            etVerificationCode.error = response.body()?.message
                        }
                        Analytics.logEvent(Analytics.Events.VERIFY, Analytics.Attributes(Analytics.AttributesKey.VERIFICATION_KEY, verificationKey),
                                Analytics.Attributes(Analytics.AttributesKey.STATUS, response.body()?.message ?: "N/A"))
                    }
                    else -> {
                        Snackbar.make(fabVerify, R.string.warning_no_internet, Snackbar.LENGTH_LONG).show()
                    }

                }
            }
        })
    }

}
