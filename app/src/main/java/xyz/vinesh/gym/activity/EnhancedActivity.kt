package xyz.vinesh.gym.activity

import android.app.Activity
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import xyz.vinesh.gym.R
import xyz.vinesh.gym.enums.Constants
import xyz.vinesh.gym.util.Analytics
import xyz.vinesh.gym.util.PrefsUtils

/**
 * Created by vineshraju on 18/10/17.
 */
abstract class EnhancedActivity : AppCompatActivity() {
    private val AUTH_REQ_CODE = 2856

    companion object {
        private var isAuthenticated = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
    }

    val kgm: KeyguardManager by lazy { getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager }
    private val deviceUnlockIntent: Intent by lazy {
        kgm.createConfirmDeviceCredentialIntent(
                "${resources.getString(R.string.title_unlock)} ${resources.getString(R.string.app_name)}",
                resources.getString(R.string.text_unlock_desc))
    }

    override fun onResume() {
        super.onResume()
        if ((System.currentTimeMillis() - PrefsUtils.getLong(Constants.LAST_AUTHENTICATED) > 60 * 1000)) {
            startActivityForResult(deviceUnlockIntent, AUTH_REQ_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTH_REQ_CODE && resultCode == Activity.RESULT_OK) {
            Analytics.logEvent(Analytics.Events.SCREEN_UNLOCKED)
            isAuthenticated = true
            PrefsUtils.put(Constants.LAST_AUTHENTICATED, System.currentTimeMillis())
        } else if (requestCode == AUTH_REQ_CODE && resultCode == Activity.RESULT_CANCELED) {
            finish()
            Analytics.logEvent(Analytics.Events.SCREEN_UNLOCKED_CANCELLED)
        }
    }

    override fun onPause() {
        super.onPause()
        if (isAuthenticated)
            PrefsUtils.put(Constants.LAST_AUTHENTICATED, System.currentTimeMillis())
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    fun onBack(view: View) {
        onBackPressed()
        val activityName = if (view.context is Activity) view.context::class.java.simpleName else "unknown"
        Analytics.logEvent(Analytics.Events.BACK_PRESSED,
                Analytics.Attributes(Analytics.AttributesKey.SCREEN, activityName)
        )
    }
}