package xyz.vinesh.gym.activity

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.widget.TextViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.telephony.PhoneNumberUtils
import android.text.format.DateUtils
import android.view.View
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import kotlinx.android.synthetic.main.activity_trainee_details.*
import kotlinx.android.synthetic.main.content_trainee_details.*
import kotlinx.android.synthetic.main.layout_quick_actions.*
import kotlinx.android.synthetic.main.layout_trainee_details_sheet.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.makeCall
import org.jetbrains.anko.sendSMS
import xyz.vinesh.gym.R
import xyz.vinesh.gym.adapter.PaymentHistoryListAdapter
import xyz.vinesh.gym.database.Database
import xyz.vinesh.gym.database.Trainee
import xyz.vinesh.gym.enums.Constants
import xyz.vinesh.gym.util.*


class TraineeDetailsActivity : EnhancedActivity() {
    private val CALL_PERMISSIONS_REQUEST = 42
    private var trainee: Trainee? = null
    private val drawableBuilder by lazy {
        TextDrawable.builder()
                .beginConfig()
                .fontSize(convertSpToPixels(20f, this)) /* size in px */
                .bold()
                .endConfig()
    }

    override fun onResume() {
        super.onResume()
        if (trainee != null) {
            trainee = Database.create(this).trainee().getBy(trainee!!.id)
            if (trainee != null) update()
        }
    }

    private fun update() {
        with(trainee!!) {
            actionBarTitle.text = "${trainee?.name}"

            val colorGenerator = ColorGenerator.MATERIAL

            val profPic = TextDrawable.builder()
                    .buildRect(name.substring(0, if (name.length > 2) 2 else 1), colorGenerator.getColor(id))

            //ivProfilePic.setImageDrawable(profPic)
            tvID.text = "#${id}"
            tvDOJ.text = "${resources.getString(R.string.title_joined)} ${DateUtils.getRelativeTimeSpanString(this@TraineeDetailsActivity, trainee!!.doj.time, true)}"
            tvPhone.text = PhoneNumberUtils.formatNumber(mobile, "IN")
            val membership = Database.create(this@TraineeDetailsActivity).membership().get(trainee!!.membershipType.toLong())
            tvMembershipTitle.text = if (membership != null) {
                membership.name + " - " + if (membership.durationInMonths <= 0) resources.getString(R.string.one_time_fee) else {
                    "${membership.durationInMonths} month${if (membership.durationInMonths > 1) "s" else ""}"
                }
            } else {
                "unknown"
            }
            if (tvMembershipTitle.text.length >= 10)
                tvMembershipTitle.setEms(10)
            tvMembershipExpiration.text = "${resources.getString(R.string.title_expires)} ${DateUtils.getRelativeTimeSpanString(this@TraineeDetailsActivity, trainee!!.expiresOn.time, true)}"

            tvPendingAmount.text = "(Pending ₹ $paymentPending)"

            ivHeight.setImageDrawable(
                    drawableBuilder.buildRound("$height", colorGenerator.getColor("h")))
            ivWeight.setImageDrawable(drawableBuilder.buildRound("$weight", colorGenerator.getColor("w")))
            ivAge.setImageDrawable(drawableBuilder.buildRound("${getAge(dob)}", colorGenerator.getColor("v")))
            ivSex.setImageDrawable(drawableBuilder.buildRound("${sex[0].toUpperCase()}", colorGenerator.getColor("d")))

            rvPaymentsHistoryList.layoutManager = LinearLayoutManager(this@TraineeDetailsActivity)
            val payments = Database.create(this@TraineeDetailsActivity).payment().getAll(trainee!!.id)
            rvPaymentsHistoryList.adapter = PaymentHistoryListAdapter(payments)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trainee_details)
        setSupportActionBar(toolbar)
        trainee = intent.extras.getParcelable(Constants.TRAINEE_DETAILS_POJO.name)
        actionBarTitle.text = "${trainee?.name}"

        update()
        ivCall.setOnClickListener {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.CALL_PHONE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE),
                        CALL_PERMISSIONS_REQUEST)
            } else makeCall(trainee!!.mobile)

            Analytics.logEvent(Analytics.Events.CALL_MEMBER)

        }
        ivText.setOnClickListener {
            sendSMS(trainee!!.mobile)
            Analytics.logEvent(Analytics.Events.TEXT_MEMBER)
        }
        ivEdit.setOnClickListener {
            if (trainee != null)
                startActivity(intentFor<TraineeEditActivity>(Constants.TRAINEE_DETAILS_POJO.name to trainee))
            Analytics.logEvent(Analytics.Events.TRAINEE_EDIT)

        }
        ivPay.setOnClickListener {
            startActivity(intentFor<PaymentActivity>(Constants.TRAINEE_DETAILS_POJO.name to trainee))
            Analytics.logEvent(Analytics.Events.PAY_CLICKED)
        }

        rvPaymentsHistoryList.setOnTouchListener { view, motionEvent ->
            val shouldClose = cvDetailssheet.visibility == View.VISIBLE
            if (shouldClose) toggleDetails()
            shouldClose
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            CALL_PERMISSIONS_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    makeCall(trainee!!.mobile)
                }
            }
        }
    }

    private fun toggleDetails() {
        if (cvDetailssheet.visibility != View.VISIBLE) {
            cvDetailssheet.slideDown()
            Analytics.logEvent(Analytics.Events.DETAILS_SHEET_DOWN)
        } else {
            cvDetailssheet.slideUp()
            Analytics.logEvent(Analytics.Events.DETAILS_SHEET_UP)
        }
    }

    fun toggleDetails(v: View) {
        toggleDetails()
    }
}
