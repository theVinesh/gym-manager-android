package xyz.vinesh.gym.activity

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_add_trainee.*
import kotlinx.android.synthetic.main.content_add_trainee.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import xyz.vinesh.gym.R
import xyz.vinesh.gym.database.Database
import xyz.vinesh.gym.database.Trainee
import xyz.vinesh.gym.enums.Constants
import xyz.vinesh.gym.util.Analytics
import xyz.vinesh.gym.util.checkAndGetValue
import xyz.vinesh.gym.util.getAge
import xyz.vinesh.gym.util.string
import xyz.vinesh.gym.view.DatePicker
import java.text.SimpleDateFormat
import java.util.*


class AddTraineeActivity : EnhancedActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_trainee)
        setSupportActionBar(toolbar)

        etDOB.setOnFocusChangeListener { view, b ->
            if (b) {
                val newFragment = DatePicker.getInstance {
                    etDOB.setText(SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(it.time))
                }
                newFragment.show(supportFragmentManager, "datePicker")
                findViewById<EditText>(etDOB.nextFocusDownId).requestFocus()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.new_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.mSave -> {
                save()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun save() {
        try {
            val id = etId.checkAndGetValue()
            val name = etName.checkAndGetValue()
            val mobile = etMobile.checkAndGetValue()
            val dob = etDOB.checkAndGetValue()

            val age = getAge(dob)

            val height = etHeight.checkAndGetValue().toFloat()
            val weight = etWeight.checkAndGetValue().toFloat()
            val sex = tvSex.text.toString()
            val doj = Calendar.getInstance().time

            val expiresOn = doj
            val paymentPending = 0f

            val trainee = Trainee(id, name, age, dob, sex, mobile, doj, -1, weight, height, expiresOn, paymentPending)
            try {
                Database.create(this).trainee().insert(trainee)
                toast("Saved")
                startActivity(intentFor<PaymentActivity>(Constants.TRAINEE_DETAILS_POJO.name to trainee))
                Analytics.logEvent(Analytics.Events.TRAINEE_ADDED)
                finish()
            } catch (e: SQLiteConstraintException) {
                longToast("Trainee with id '$id' or mobile '$mobile' already exist")
                Analytics.logEvent(Analytics.Events.DUPLICATE,
                        Analytics.Attributes(Analytics.AttributesKey.ACTION, string(R.string.title_activity_add_trainee))
                )
            }
        } catch (e: UnsupportedOperationException) {
            longToast(e.message ?: resources.getString(R.string.error_default))
            Analytics.logEvent(Analytics.Events.INCOMPLETE_FORM,
                    Analytics.Attributes(Analytics.AttributesKey.ACTION, string(R.string.title_activity_add_trainee))
            )
        }
    }

    fun toggleSex(view: View) {
        if (tvSex.text.toString() == "M") tvSex.text = "F" else tvSex.text = "M"
    }
}
