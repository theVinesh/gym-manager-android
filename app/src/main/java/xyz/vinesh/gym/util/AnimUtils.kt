package xyz.vinesh.gym.util

import android.view.View
import android.view.ViewAnimationUtils
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.Transformation


/**
 * Created by vineshraju on 23/12/17.
 */
fun View.circularReveal() {
    with(this) {
        val cx = width / 2
        val cy = height / 2
        ViewAnimationUtils.createCircularReveal(this, cx, cy, 0f,
                Math.hypot(cx.toDouble(), cy.toDouble()).toFloat()
        ).start()
    }
}

fun View.slideDown() {
    this.measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
    val targetHeight = measuredHeight

    layoutParams.height = 1
    visibility = View.VISIBLE
    val a = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            layoutParams.height = if (interpolatedTime == 1f)
                WindowManager.LayoutParams.WRAP_CONTENT
            else
                (targetHeight * interpolatedTime).toInt()
            requestLayout()
        }

        override fun willChangeBounds(): Boolean {
            return true
        }
    }

    a.duration = (targetHeight / context.resources.displayMetrics.density).toLong()
    startAnimation(a)
}

fun View.slideUp() {
    val initialHeight = measuredHeight

    val a = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            if (interpolatedTime == 1f) {
                visibility = View.GONE
            } else {
                layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                requestLayout()
            }
        }

        override fun willChangeBounds(): Boolean {
            return true
        }
    }

    a.duration = (initialHeight / context.resources.displayMetrics.density).toLong()
    startAnimation(a)
}