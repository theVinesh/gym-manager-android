package xyz.vinesh.gym.util

import android.preference.PreferenceManager
import net.idik.lib.cipher.so.CipherClient
import xyz.vinesh.gym.App
import xyz.vinesh.gym.enums.Constants
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.spec.SecretKeySpec

/**
 * Created by vineshraju on 18/10/17.
 */
object PrefsUtils {
    private val prefManager = PreferenceManager.getDefaultSharedPreferences(App.context)
    private var aesKey = SecretKeySpec(CipherClient.key().toByteArray(), "AES")
    private var cipher = Cipher.getInstance("AES/ECB/ZeroBytePadding")
    fun put(key: Constants, value: Any) {
        put(key.name, value)
    }

    fun put(key: String, value: Any) {
        val editor = prefManager.edit()
        when (value) {
            is String -> {
                //val encryptedValue = encrypt(value)
                editor.putString(key, value)
            }
            is Int -> editor.putInt(key, value)
            is Boolean -> editor.putBoolean(key, value)
            is Long -> editor.putLong(key, value)
        }
        editor.apply()
    }

    fun getString(key: String) = prefManager.getString(key, "")//decrypt(prefManager.getString(key, ""))
    fun getInt(key: String) = prefManager.getInt(key, -1)
    fun getLong(key: String) = prefManager.getLong(key, -1)
    fun getBoolean(key: String) = prefManager.getBoolean(key, false)

    fun getString(key: Constants) = getString(key.name)
    fun getInt(key: Constants) = getInt(key.name)
    fun getLong(key: Constants) = getLong(key.name)
    fun getBoolean(key: Constants) = getBoolean(key.name)

    fun encrypt(value: String): String {
        cipher.init(Cipher.ENCRYPT_MODE, aesKey)
        return String(cipher.doFinal(value.toByteArray()))
    }

    fun decrypt(value: String) = try {
        cipher.init(Cipher.DECRYPT_MODE, aesKey)
        String(cipher.doFinal(value.toByteArray()))
    } catch (e: IllegalBlockSizeException) {
        "UNENCRYPTED"
    }

}