package xyz.vinesh.gym.util

import android.content.Context
import android.widget.EditText
import xyz.vinesh.gym.R

/**
 * Created by vineshraju on 23/12/17.
 */
fun EditText.checkAndGetValue(): String {
    if (this.text.isEmpty()) {
        this.error = resources.getString(R.string.error_cannot_be_empty)
        throw UnsupportedOperationException(resources.getString(R.string.error_fill_in_fields))
    }
    return this.text.toString()
}

fun Context.string(id: Int) = resources.getString(id)