package xyz.vinesh.gym.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by vineshraju on 24/12/17.
 */
fun getAge(dob: String): Int {
    val calDOB = dob.parseDate()

    val today = Calendar.getInstance()
    var age = today.get(Calendar.YEAR) - calDOB.get(Calendar.YEAR)

    if (today.get(Calendar.DAY_OF_YEAR) < calDOB.get(Calendar.DAY_OF_YEAR)) {
        age--
    }
    return age
}

fun String.parseDate(): Calendar {
    val cal = Calendar.getInstance()
    try {
        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
        cal.time = sdf.parse(this)

    } catch (e: ParseException) {
        //returning current date
    }
    return cal
}