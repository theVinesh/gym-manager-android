package xyz.vinesh.gym.view

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.layout_add_membership_dialog.view.*
import xyz.vinesh.gym.R
import xyz.vinesh.gym.database.Database
import xyz.vinesh.gym.database.Membership
import xyz.vinesh.gym.enums.Constants
import xyz.vinesh.gym.util.Analytics
import xyz.vinesh.gym.util.checkAndGetValue

/**
 * Created by vineshraju on 13/11/17.
 */
class AddMembershipDialog : DialogFragment() {
    var onDismiss: () -> Unit = {}

    companion object {
        fun getInstance(membership: Membership? = null): AddMembershipDialog {
            val addMembershipDialog = AddMembershipDialog()
            val bundle = Bundle()
            bundle.putParcelable(Constants.MEMBERSHIP_DETAILS_POJO.name, membership)
            addMembershipDialog.arguments = bundle
            return addMembershipDialog
        }
    }

    fun setOnDismissListener(onDismissCallback: () -> Unit) {
        onDismiss = onDismissCallback
    }

    override fun onResume() {
        val params = dialog.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = params as android.view.WindowManager.LayoutParams

        super.onResume()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.layout_add_membership_dialog, container)
        with(view) {
            val membership = arguments?.getParcelable<Membership>(Constants.MEMBERSHIP_DETAILS_POJO.name)
            if (membership != null) {
                etMembershipName.setText(membership.name)
                etDuration.setText(membership.durationInMonths.toString())
                etPrice.setText(membership.price.toString())
            }
            bAdd.setOnClickListener {
                try {
                    val name = etMembershipName.checkAndGetValue()
                    val duration = etDuration.checkAndGetValue().toInt()
                    val price = etPrice.checkAndGetValue().toFloat()
                    if (membership != null) {
                        membership.name = name
                        membership.durationInMonths = duration
                        membership.price = price
                        Database.create(context).membership().update(membership)
                    } else {
                        Database.create(context).membership().insert(Membership(null, name, price, duration))
                    }
                    Analytics.logEvent(Analytics.Events.ADD_MEMBERSHIP_PLAN,
                            Analytics.Attributes(Analytics.AttributesKey.SCREEN, this@AddMembershipDialog::class.java.simpleName)
                    )
                    onDismiss()
                    dismiss()
                } catch (e: UnsupportedOperationException) {

                }
            }
        }
        return view
    }
}