package xyz.vinesh.gym.networking

import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import xyz.vinesh.gym.networking.responses.APIResponse

/**
 * Created by vineshraju on 1/10/17.
 */
interface APIs {
    @FormUrlEncoded
    @POST("verifyProductKey")
    fun verifyProductKey(@Field("key") key: String): Call<APIResponse>

    @FormUrlEncoded
    @POST("updateProductKeyUsage")
    fun updateProductKeyUsage(@Field("key") key: String): Call<APIResponse>
}




