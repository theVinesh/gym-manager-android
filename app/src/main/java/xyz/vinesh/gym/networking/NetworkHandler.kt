package xyz.vinesh.gym.networking

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by vineshraju on 1/10/17.
 */
class NetworkHandler {
    companion object {
        private val API_BASE_URL = "https://us-central1-gym-manager-66633.cloudfunctions.net/"
        //private val API_BASE_URL = "http://10.0.2.2:5000/gym-manager-66633/us-central1/"

        private val client: Retrofit by lazy {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor { chain ->
                val original = chain.request()
                val request = original.newBuilder()
                        //.header("Authorization", "Basic c3ViYXM6c3ViYXM=")
                        .method(original.method(), original.body())
                        .build()

                chain.proceed(request)
            }
            httpClient.addInterceptor(logging)


            Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build()
        }

        fun create() = client.create(APIs::class.java)!!
    }

}