package xyz.vinesh.gym.enums

/**
 * Created by vineshraju on 18/10/17.
 */
enum class PaymentMethods {
    CARD, CASH
}