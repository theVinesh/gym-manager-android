package xyz.vinesh.gym.enums

/**
 * Created by vineshraju on 18/10/17.
 */
enum class Constants {
    //shared pref keys
    LAST_AUTHENTICATED,
    //intent keys
    TRAINEE_DETAILS_POJO,
    MEMBERSHIP_DETAILS_POJO,
    //db collection constants
    verification_keys,
    gym_name
}