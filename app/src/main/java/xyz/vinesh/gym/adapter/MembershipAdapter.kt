package xyz.vinesh.gym.adapter

import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.layout_membership_row.view.*
import xyz.vinesh.gym.R
import xyz.vinesh.gym.database.Membership

/**
 * Created by vineshraju on 31/10/17.
 */
class MembershipAdapter(val memberships: List<Membership>, val onCLick: (membership: Membership) -> Unit, val onDelete: (membership: Membership) -> Unit) : RecyclerView.Adapter<MembershipAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bind(position = position, membership = memberships[position], onCLick = onCLick, onDelete = onDelete)
    }

    override fun getItemCount() = memberships.size
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int) =
            ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.layout_membership_row, parent, false))


    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int, membership: Membership? = null,
                 onCLick: (membership: Membership) -> Unit,
                 onDelete: (membership: Membership) -> Unit) {
            with(itemView) {
                if (position % 2 == 0) {
                    itemView.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
                } else {
                    itemView.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.dividerColor, null))
                }
                tvName.text = membership!!.name + " - " + if (membership.durationInMonths <= 0) resources.getString(R.string.one_time_fee) else {
                    "${membership.durationInMonths} month${if (membership.durationInMonths > 1) "s" else ""}"
                }
                imageView.setOnClickListener {
                    onDelete(membership)
                }
                setOnClickListener { onCLick(membership) }

            }
        }
    }
}